function readstatus(txt){
var collect=new Array;
var cobj={};
var lines=txt.split("\n");
for (var i=0; lines.length > i; i++){
var aline=lines[i];
if(aline.indexOf(':') > 0){
    var ipos = aline.indexOf(":");
    var name = aline.substring(0,ipos).trim();
    var value = aline.substring(ipos+1).trim();
/* parses Flags */
    if(name == 'Flags'){
	var flags = value.split(' ');
	var cflag = {};
	for (iflag=0; flags.length > iflag; iflag++){
	    var ipos = flags[iflag].indexOf('=');
	    var fname=flags[iflag].substring(0,ipos).trim();
	    var fvalue;
	    if(ipos>1){
		fvalue=flags[iflag].substring(ipos+1);
	    }
	    else
	    {
		fvalue=true;
	    }
	    cflag[fname]=fvalue;
	}
	value=cflag;
    }
if(name == 'Parent'){
cobj={};
collect.push(cobj);
}
cobj[name]=value;
}
}
var output= JSON.stringify(collect);
var mtime = (new Date).toISOString();
var montime = document.getElementById('monitortime');
if(montime){
montime.lastElementChild.innerHTML=mtime;
}
return output;
};
